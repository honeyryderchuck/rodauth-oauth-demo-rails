# Rodauth Rails Demo

Example app that demonstrates how to integrate [rodauth-oauth] OAuth/OIDC
framework into Rails using the [rodauth-rails] gem.

Used in the blog post ["How to build an OIDC provider using rodauth-oauth on Rails"](https://honeyryderchuck.gitlab.io/httpx/2021/03/15/oidc-provider-on-rails-using-rodauth-oauth.html).


## tl;dr for quick demonstration

### 1. start the rails application

First, clone this repository locally. Then execute the following commands:

```bash
> bundle install
> bundle exec rails db:create (make sure you have "postgresql" installed)
> bundle exec rails db:migrate
> bundle exec rails server
```

## 2. run the webpacker server

Just so that it looks nice.

```bash
> bundle exec rails webpacker:install
> bundle exec bin/webpack-dev-server
```

## 3. create an account

Go to `http://localhost:3000` and create a user account. The only thing you **must** do is assign the `"foo@bar.com"` email to it.

(don't forget to verify the account; you should get the link in the logs).

To make the demo more interesting, go to the "Posts" page and just create a few.

## 4. create a client application

Run the following curl script:

```bash
> curl -X POST http://localhost:3000/register \
  -F "client_name=Client Posts App" \
  -F "description=A app showing my posts from this one" \
  -F "client_uri=http://localhost:9293" \
  -F "redirect_uris[]=http://localhost:9293/auth/openid_connect/callback" \
  -F "scopes=openid email profile posts.read"
```

copy the `"client_id"` and `"client_secret"` values somewhere.

## 5. run the client application

Set the client id and client secret from the previous step in the CLIENT_ID and CLIENT_SECRET env vars.

```bash
> export CLIENT_ID=${client_id}
> export CLIENT_SECRET=${client_secret}
> export RESOURCE_SERVER_URI=http://localhost:3000/posts
> export AUTHORIZATION_SERVER_URI=http://localhost:3000
> bundle exec ruby scripts/client_application.rb
```

## 5. Try out the demo!

Go to `http://localhost:9293`; authenticate using OpenID. You should see user info + the list of posts afterwards.

[Rodauth]: https://github.com/jeremyevans/rodauth
[rodauth-rails]: https://github.com/janko/rodauth-rails
[rodauth-oauth]: https://gitlab.com/honeyryderchuck/rodauth-oauth
